$(document).ready(function() {
    $(".testimonial-carousel").slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        arrows: true,
        // accessibility: true,
        // dots: true,
        dotsClass: 'slick-dots',
        prevArrow: $(".slick-prev"),
        nextArrow: $(".slick-next")
    });
});